const mongoose = require("mongoose");

const QuizQuestionsSchema = new mongoose.Schema({
  difficulty: String,
  question: String,
  answers: [String],
  correctAnswer: String,
});

const QuizQuestions = mongoose.model("QuizQuestions", QuizQuestionsSchema);

module.exports = QuizQuestions;
