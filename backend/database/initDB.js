// Importing the database module
const db = require("./database.js");
const acids = require("./acids.json");
const questions = require("./quiz-questions.json");

//SOLID principle
//Data oriented design

// Connecting to the database and inserting the amino acid
db.connectDB()
  .then(() => {
    db.insertAminoAcid(...acids)
      .then(() => {
        console.log("Amino acids successfully inserted.");
      })
      .catch((error) => {
        console.error("Failed to insert amino acids", error);
      });
    db.insertQuestion(...questions)
      .then(() => {
        console.log("Questions successfully inserted.");
      })
      .catch((error) => {
        console.error("Failed to insert Questions", error);
      });
  })
  .catch((error) => {
    console.error("Failed to connect to database", error);
  });

module.exports = acids;
