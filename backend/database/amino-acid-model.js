const mongoose = require("mongoose");

const AminoAcidSchema = new mongoose.Schema({
  name: { type: String, required: true },
  codons: [String],
  properties: {
    formula: String,
    molecularWeight: Number,
    classification: String,
    polarity: String,
    charge: String,
    hydropathy: Number,
  },
  chemicalProperties: {
    hydrogen: Number,
    oxygen: Number,
    nitrogen: Number,
    carbon: Number,
  },
  structure: String,
});

const AminoAcid = mongoose.model("AminoAcid", AminoAcidSchema);

module.exports = AminoAcid;
