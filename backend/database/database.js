// Importing dotenv for environment variables and Mongoose for MongoDB interactions
// require("dotenv").config({path: './database/.env'});
const mongoose = require("mongoose");

const dotenv = require("dotenv").config({ path: "../.env" });
// Fetching the connection string from environment variables
const dbUri = process.env.MONGODB_URI;

// Create AminoAcid model from the schema
const AminoAcid = require("./amino-acid-model");

const Question = require("./quiz-model");

// This is an asynchronous function that will connect to the MongoDB database
async function connectDB() {
  try {
    // Mongoose provides a connect() method for establishing a connection
    // If there was an error connecting to the database, it will be logged here
    await mongoose
      .connect(dbUri)
      .then(() =>
        console.log(
          "Connected to database" + "; Database Name:",
          mongoose.connection.name
        )
      );
  } catch (error) {
    console.error("Failed to connect to database", error);
  }
}

// Insert an aminoacid into the AminoAcids collection
async function insertAminoAcid(...aminoAcids) {
  try {
    for (let aminoAcid of aminoAcids) {
      await AminoAcid.findOneAndUpdate(
        {
          name: aminoAcid.name,
        },
        aminoAcid,
        { upsert: true, new: true, overwrite: true }
      );
      // const newAminoAcid = new AminoAcid(aminoAcid); // Creating an instance of the model
      // const err = await newAminoAcid.save(); // Using save() method of the model instance to store the document
      // console.log(err);
    }
    console.log("Amino acids inserted successfully");
  } catch (error) {
    console.error("Failed to insert amino acids", error);
    throw error;
  }
}

// Insert an aminoacid into the AminoAcids collection
async function insertQuestion(...questions) {
  try {
    for (let question of questions) {
      await Question.findOneAndUpdate(
        {
          question: question.question,
        },
        question,
        { upsert: true, new: true, overwrite: true }
      );
      // const newAminoAcid = new AminoAcid(aminoAcid); // Creating an instance of the model
      // const err = await newAminoAcid.save(); // Using save() method of the model instance to store the document
      // console.log(err);
    }
    console.log("Questions inserted successfully");
  } catch (error) {
    console.error("Failed to insert questions", error);
    throw error;
  }
}

// Asynchronously fetch all amino acids from the database
async function getAminoAcids() {
  try {
    const aminoAcids = await AminoAcid.find({});
    return aminoAcids;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

// Asynchronously fetch all questions from the database
async function getQuestions() {
  try {
    const questions = await Question.find({});
    return questions;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function getAminoAcidByName(name) {
  try {
    const aminoAcid = await AminoAcid.findOne({ name: name });
    return aminoAcid;
  } catch (err) {
    console.error(err);
  }
}

// This is an asynchronous function that will close the connection to the MongoDB database
async function closeDB() {
  try {
    // Mongoose provides a disconnect() method to close the connection
    // If there was an error disconnecting from the database, it will be logged here
    await mongoose.disconnect();
    console.log("Disconnected from database");
  } catch (error) {
    console.error("Failed to disconnect from database", error);
  }
}

// Exporting the connectDB, insertAminoAcid, and closeDB functions so that they can be used in other modules
module.exports = {
  connectDB,
  insertAminoAcid,
  closeDB,
  getAminoAcids,
  getAminoAcidByName,
  insertQuestion,
  getQuestions,
};
