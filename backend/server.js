const express = require("express");
const app = express();
const connectDB = require("./database/database.js");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");
const debug = require("debug");
const config = require("config");
const routes = require("./routes/index.js");
require("./database/initDB.js");

// Connect to the database and handle any potential connection errors
connectDB
  .connectDB()
  .then(() => console.log("Database connected successfully"))
  .catch((error) => console.error("Failed to connect to the database:", error));

// Config
console.log("Application Name: " + config.get("name"));
console.log("Mode: " + config.get("mode"));
console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
console.log(`app: ${app.get("env")}`);

app.use(express.json()); // Use middleware to parse incoming JSON request bodies
app.use(helmet()); // Use helmet
app.use(cors()); // Use CORS to allow requests from your React app's domain
app.use(function (request, response, next) {
  console.log(
    `[${new Date().toLocaleTimeString()}] ${request.method} ${
      request.originalUrl
    }`
  );
  next();
});
app.use(routes); // Use the roots

if (app.get("env") === "development") {
  app.use(morgan("tiny"));
  debug("Morgan enabled...");
}
// if app is in development state I will use morgan to see http requests data
//  use "export NODE_ENV=production" or "export NODE_ENV=development" to switch between the two
// to use the debug function type "export DEBUG=app:*" and then "nodemon server.js" in the CLI

// Start the server
const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

process.on("SIGINT", async () => {
  await connectDB.closeDB();
  process.exit(1);
});
