const express = require("express");
const router = express.Router();
const { getQuestions } = require("../database/database");

// GET request to '/questions'
router.get("/", async (req, res) => {
  try {
    const questions = await getQuestions();
    res.json(questions);
  } catch (error) {
    console.error("Error fetching questions:", error);
    res.status(500).send("Error fetching questions");
  }
});

module.exports = router;
