const express = require("express");
const router = express.Router();
const {
  getAminoAcids,
  getAminoAcidByName,
  insertAminoAcid,
} = require("../database/database.js");

// GET request to '/api/aminoacids'
router.get("/", async (req, res) => {
  try {
    const aminoAcids = await getAminoAcids();
    res.json(aminoAcids);
  } catch (error) {
    console.error("Error fetching amino acids:", error);
    res.status(500).send("Error fetching amino acids");
  }
});

// GET request to '/api/aminoacids/:name'
router.get("/:name", async (req, res) => {
  try {
    const aminoAcid = await getAminoAcidByName(req.params.name);
    if (aminoAcid) {
      res.json(aminoAcid);
    } else {
      res.status(404).send("Amino Acid not found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
});

// POST request to '/api/aminoacids'
router.post("/", async (req, res) => {
  const newAminoAcids = req.body;
  try {
    await insertAminoAcid(...newAminoAcids);
    res.status(201).send("Amino acids inserted");
  } catch (err) {
    res.status(500).send(err.message);
  }
});

module.exports = router;
