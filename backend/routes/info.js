const express = require("express");
const router = express.Router();
const db = require("../database/database.js"); // Include database functions

router.get("/aminoacids", async (req, res) => {
  try {
    const aminoAcids = await db.getAminoAcids();
    res.json(aminoAcids);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/aminoacids/:name", async (req, res) => {
  try {
    const aminoAcid = await db.getAminoAcidByName(req.params.name);
    res.json(aminoAcid);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
