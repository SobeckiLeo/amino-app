const express = require("express");
const router = express.Router();

// Importing individual routes
const quizRoutes = require("./quiz");
const infoRoutes = require("./info");
const aminoAcidsRoutes = require("./aminoAcids");

// Using the individual routes
router.use("/quiz", quizRoutes);
router.use("/info", infoRoutes);
router.use("/aminoacids", aminoAcidsRoutes);

module.exports = router;

// In routes / index.js, I'm importing individual route groupings
// and assigning them to their respective endpoints under / api.
