import React from "react"; // Importing React library for building user interfaces.
import ReactDOM from "react-dom"; // Importing ReactDOM library for DOM rendering.
import "./index.css"; // Importing global styles.
import ReactApp from "./App"; // Importing the main application component.
import reportWebVitals from "./reportWebVitals"; // Import function for reporting web vitals.

// Getting the root DOM node where the React app will be attached.
const root = document.getElementById("root");

// Here I'm rendering the React application into the root DOM node using concurrent mode.
// StrictMode checks for potential problems during development.
// Rendering the main application component with an initial component as "InfoComponent."
ReactDOM.createRoot(root).render(
  <React.StrictMode>
    <ReactApp initialComponent="InfoComponent" />
  </React.StrictMode>
);

// Calling the reportWebVitals function with console.log to report web vitals for performance analysis.
reportWebVitals(console.log);
