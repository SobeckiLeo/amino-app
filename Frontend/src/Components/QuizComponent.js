import React, { useState, useEffect } from "react";
import "../index.css";

// This code defines a QuizApp component that represents a quiz application.
// The quiz includes a set of questions and answers, functionality to shuffle questions,
// navigate through them, provide feedback on correct/incorrect answers, and display a final score.
// I'm including a button to switch to a search component, using a prop function called switchComponent.

export default function QuizApp({ switchComponent }) {
  // Defining the QuizApp component, accepting a 'switchComponent' prop to switch views to InfoComponent and back.

  // This function shuffles the array, to randomize the order of quiz questions each time the quiz is started.

  function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  const [quizData, setQuizData] = useState([]);
  // Initializing the quizData state with the shuffled initial data.

  const [currentQuestion, setCurrentQuestion] = useState(0);
  // currentQuestion keeps track of which question the user is on.

  const [showScore, setShowScore] = useState(false);
  // showScore determines if the final score should be displayed.

  const [score, setScore] = useState(0);
  // score keeps track of the user's score.

  const [showAnswer, setShowAnswer] = useState(false);
  // showAnswer determines if feedback should be displayed after answering.

  const [isCorrectAnswer, setIsCorrectAnswer] = useState(null);
  // isCorrectAnswer keeps track of whether the last answer was correct.

  const [selectedDifficulty, setSelectedDifficulty] = useState("easy");

  // Whenever the user changes the difficulty, this useEffect will shuffle and filter the questions accordingly.
  useEffect(() => {
    const getQuiz = async () => {
      const response = await fetch("/quiz");
      const initialQuizData = await response.json();
      const filteredQuestions = initialQuizData.filter(
        (q) => q.difficulty === selectedDifficulty
      );
      setQuizData(shuffleArray(filteredQuestions));
      setCurrentQuestion(0);
    };
    getQuiz();
  }, [selectedDifficulty]);

  const handleAnswerOptionClick = (isCorrect) => {
    setIsCorrectAnswer(isCorrect);
    if (isCorrect) setScore(score + 1);
    setShowAnswer(true);
  };

  const handleNextQuestion = () => {
    const nextQuestion = currentQuestion + 1;
    if (nextQuestion < quizData.length) {
      setCurrentQuestion(nextQuestion);
    } else {
      setShowScore(true);
    }
    setShowAnswer(false);
  };

  const restartQuiz = () => {
    setQuizData(shuffleArray(quizData));
    setCurrentQuestion(0);
    setShowScore(false);
    setScore(0);
  };

  const handleDifficultyChange = (event) => {
    setSelectedDifficulty(event.target.value);
  };

  return (
    <div className="quiz-app">
      <h1>Amino Acid Quiz</h1>

      {/* Dropdown to select quiz difficulty */}
      <select value={selectedDifficulty} onChange={handleDifficultyChange}>
        <option value="easy">Easy</option>
        <option value="medium">Medium</option>
        <option value="difficult">Difficult</option>
      </select>

      {/* Button to switch views */}
      <button
        className="switch-button"
        onClick={() => switchComponent("InfoComponent")}
      >
        Go to the search
      </button>

      {showScore ? (
        <div className="score-section">
          You scored {score} out of {quizData.length}
          <button onClick={restartQuiz}>Retake Quiz</button>
        </div>
      ) : showAnswer ? (
        <div className="feedback-section">
          {isCorrectAnswer ? "Correct! Great job!" : "Incorrect! Keep trying!"}
          <div>
            The correct answer was: {quizData[currentQuestion].correctAnswer}
          </div>
          <button onClick={handleNextQuestion}>Next Question</button>
        </div>
      ) : quizData.length === 0 ? (
        <>
          <h2>Loading...</h2>
        </>
      ) : (
        <>
          <div className="question-section">
            <div className="question-count">
              <span>Question {currentQuestion + 1}</span>/{quizData.length}
              <span> | Current Score: {score}</span>
            </div>
            <div className="question-text">
              {quizData[currentQuestion].question}
            </div>
          </div>
          <div className="answer-section">
            {quizData[currentQuestion].answers.map((answerOption, index) => (
              <button
                key={index}
                onClick={() =>
                  handleAnswerOptionClick(
                    answerOption === quizData[currentQuestion].correctAnswer
                  )
                }
              >
                {answerOption}
              </button>
            ))}
          </div>
        </>
      )}
    </div>
  );
}
