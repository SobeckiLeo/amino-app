import * as React from "react";

// Defining a functional component named 'Footer'.

export default function Footer() {
  return (
// I'm  rendering static text that serves as a footer is called from App.js
    <div>
        This project was coded by{" "}
          <a
            href="https://objective-noether-fbbdfd.netlify.app/index.html"
            target="_blank"
            rel="noopener noreferrer"
          >
            Leo Sobecki
          </a>{" "}
          and is{" "}
          <a
            href="https://gitlab.com/SobeckiLeo/amino-app/"
            target="_blank"
            rel="noopener noreferrer"
          >
            available on GitLab
          </a>{" "}
          and{" "}
          <a href="" target="_blank" rel="noopener noreferrer">
            hosted on Netlify
          </a>
     </div>
  );
}