import React, { useEffect, useState } from "react";
import AminoAcid from "./AminoAcid";
import axios from "axios"; // I'm importing axios to make HTTP requests

const AminoAcidsList = () => {
  // I'm defining a state variable 'aminoAcids' to hold the list of amino acids, initialized as an empty array.
  const [aminoAcids, setAminoAcids] = useState([]);

  // I'm using the useEffect hook to make a request to fetch the amino acids data when the component mounts.
  useEffect(() => {
    // I'm making a GET request to the '/aminoacids' endpoint to get the amino acids list.
    axios
      .get("/aminoacids")
      .then((response) => {
        // I'm updating the 'aminoAcids' state with the fetched data.
        setAminoAcids(response.data);
        console.log("Data fetched");
      })
      .catch((error) => {
        // I'm logging an error message if there's a problem fetching the data.
        console.error("Error fetching data: ", error);
      });
  }, []);

  // I'm returning the JSX to render the list of amino acids.
  return (
    <div>
      {/* I'm iterating over the 'aminoAcids' array and rendering an 'AminoAcid' component for each amino acid.
          The 'key' prop tells React which items have changed, and I'm spreading the amino acid object to pass all of its properties as props. */}
      {aminoAcids.map((aminoAcid, index) => (
        <AminoAcid key={index} {...aminoAcid} />
      ))}
    </div>
  );
};

export default AminoAcidsList;
