import React from "react";
import { Route, Routes } from "react-router-dom";
import AminoAcid from "./AminoAcid";
import AminoAcidList from "./AminoAcidList";
import ComboBox from "./ComboBox";
import QuizComponent from "./QuizComponent";
import InfoComponent from "./InfoComponent";

// I'm defining a functional component that holds all of the client-side routes for the application.
const ClientRoutes = () => {
  return (
    <Routes>
      {/* I'm defining routes that map to the component when the URL matches the path. */}
      <Route path="/aminoacids/ComboBox" element={<ComboBox />} />
      <Route path="/quiz" element={<QuizComponent />} />
      <Route path="/info" element={<InfoComponent />} />
      <Route path="/" element={<ComboBox />} />
      <Route path="/aminoacids" element={<AminoAcidList />} />
      {/* I'm defining a route that maps to the AminoAcid component when the URL includes a dynamic parameter ":aminoAcid". This allows the display of details for a specific amino acid. */}
      <Route path="/:aminoAcid" element={<AminoAcid />} />
    </Routes>
  );
};

export default ClientRoutes;
