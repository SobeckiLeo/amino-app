import React from "react";
// import SearchEngine from "./SearchEngine";
// // I'm importing the SearchEngine component, but it's not used yet

const InfoComponent = ({ switchComponent }) => {
  // I'm defining a functional component named InfoComponent that takes a prop named switchComponent.
  // This prop is expected to be a function that will be called to switch to another component, like the quiz.
  return (
    <div className="info-component">
     {/* I'm returning a div element with a class name 'info-component' which can be used for styling this component. */}

      {/* Inside the div, I'm rendering a button element with the class name 'switch-button' for styling purposes.
          When this button is clicked, I'm calling the switchComponent function passed as a prop and passing "QuizComponent" as an argument.
          This triggers a switch to the QuizComponent  */}
      <button
        className="switch-button"
        onClick={() => switchComponent("QuizComponent")}
      >
        Go to the quiz
      </button>
    </div>
  );
};

// I'm exporting the InfoComponent to make it available for use in other parts of the application.
export default InfoComponent;
