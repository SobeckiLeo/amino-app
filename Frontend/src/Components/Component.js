import React from "react";

const Component = ({ children }) => {
  // I'm defining a functional component that accepts children as a prop.
  // With the "children" prop I will pass React components/elements as children to this component.

  // After that I'm returning a single <div> element that wraps the children,
  // so that whatever is passed to this Component as children will be rendered inside this <div>.
  return <div>{children}</div>;
};

export default Component;