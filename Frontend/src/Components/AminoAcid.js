import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

// I'm defining a functional component called AminoAcid.
const AminoAcid = () => {
  // I'm using the useParams hook to get the 'aminoAcid' param from the URL.
  const params = useParams();

  // I'm defining a state variable 'acid' to hold information about the amino acid.
  // The default state includes properties like name, properties, codons, structure, and chemical properties.
  const [acid, setAcid] = useState({
    name: "",
    properties: {},
    codons: [],
    structure: "",
    chemicalProperties: {},
  });

  // I'm defining a state variable 'error' to capture any errors during the data fetching process.
  const [error, setError] = useState(null);

  // I'm using the useEffect hook to fetch the amino acid data when the component mounts.
  useEffect(() => {
    // I'm making an HTTP GET request to the server to get the amino acid details.
    axios
      .get(`/aminoacids/${params.aminoAcid}`)
      .then((response) => {
        // I'm updating the 'acid' state with the fetched data.
        setAcid(response.data);
        console.log("Data fetched");
      })
      .catch((error) => {
        // I'm logging and setting the error if there's a problem fetching the data.
        console.error("Error fetching data: ", error);
        setError(error);
      });
  }, []);

  // If there's an error, I'm returning an error message.
  if (error) {
    return <div>Error: {error.message}</div>;
  }

  // I'm checking if the required properties are provided, if not, I'm displaying a loading message.
  if (!acid.name) {
    return <div>Loading...</div>;
  }

  // I'm returning the JSX to render the details of the amino acid.
  return (
    <div>
      <h2>{acid.name}</h2>

      <h3>Properties</h3>
      {/* I'm iterating over the properties of the acid and displaying them. */}
      {acid.properties &&
        Object.entries(acid.properties).map(([key, value]) => (
          <p key={key}>
            <strong>{key}:</strong> {value}
          </p>
        ))}

      <h3>Codons</h3>
      {/* I'm displaying the codons as a comma-separated list. */}
      <p>{acid.codons.join(", ")}</p>

      <h3>Structure</h3>
      {/* I'm displaying the structure of the acid as an image if it's available. */}
      {acid.structure && (
        <img src={acid.structure} alt={`${acid.name} structure`} />
      )}

      <h3>Chemical Properties</h3>
      {/* I'm iterating over the chemical properties of the acid and displaying them. */}
      {acid.chemicalProperties &&
        Object.entries(acid.chemicalProperties).map(([key, value]) => (
          <p key={key}>
            <strong>{key}:</strong> {value}
          </p>
        ))}
    </div>

  );
};

export default AminoAcid;
