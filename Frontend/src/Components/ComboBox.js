import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { useNavigate } from "react-router-dom";

// I'm defining a constant array containing the details of amino acids 
// that I'm later passing to the Autocomplete in options 

const AminoAcids = [
  { name: "Alanine", threeLetterCode: "Ala", oneLetterCode: "A" },
  { name: "Cysteine", threeLetterCode: "Cys", oneLetterCode: "C" },
  { name: "Aspartic Acid", threeLetterCode: "Asp", oneLetterCode: "D" },
  { name: "Glutamic Acid", threeLetterCode: "Glu", oneLetterCode: "E" },
  { name: "Phenylalanine", threeLetterCode: "Phe", oneLetterCode: "F" },
  { name: "Glycine", threeLetterCode: "Gly", oneLetterCode: "G" },
  { name: "Histidine", threeLetterCode: "His", oneLetterCode: "H" },
  { name: "Isoleucine", threeLetterCode: "Ile", oneLetterCode: "I" },
  { name: "Lysine", threeLetterCode: "Lys", oneLetterCode: "K" },
  { name: "Leucine", threeLetterCode: "Leu", oneLetterCode: "L" },
  { name: "Methionine", threeLetterCode: "Met", oneLetterCode: "M" },
  { name: "Asparagine", threeLetterCode: "Asn", oneLetterCode: "N" },
  { name: "Proline", threeLetterCode: "Pro", oneLetterCode: "P" },
  { name: "Glutamine", threeLetterCode: "Gln", oneLetterCode: "Q" },
  { name: "Arginine", threeLetterCode: "Arg", oneLetterCode: "R" },
  { name: "Serine", threeLetterCode: "Ser", oneLetterCode: "S" },
  { name: "Threonine", threeLetterCode: "Thr", oneLetterCode: "T" },
  { name: "Valine", threeLetterCode: "Val", oneLetterCode: "V" },
  { name: "Tryptophan", threeLetterCode: "Trp", oneLetterCode: "W" },
  { name: "Tyrosine", threeLetterCode: "Tyr", oneLetterCode: "Y" },
];

export default function ComboBox() {
  // I'm using the `useNavigate` hook from 'react-router-dom' to navigate to different routes
  const navigate = useNavigate();

  return (
    // Rendering an Autocomplete component from the Material-UI library to provide a dropdown selection list.
    <Autocomplete
      disablePortal
      id="combo-box-demo"
    // Setting the options for the dropdown from the 'AminoAcids' array defined earlier
      options={AminoAcids}
   // Applying styling to the component
      sx={{
        width: "100%",
        background: "white",
        fontFamily: "Verdana",
        fontSize: 16,
      }}
      renderInput={(params) => (
      // I'm rendering a TextField as the input element and labeling it "Choose Amino Acid".
        <TextField
          {...params}
          label="Choose Amino Acid"
          sx={{
            "& .MuiDrawer-paper": {
              background: "#234668",
            },
          }}
        />
      )}
      // I'm customizing the rendering of each option in the dropdown with black text and the "Verdana" font.
      renderOption={(props, option) => (
        <li {...props} style={{ color: "black", fontFamily: "Verdana" }}>
          {option.name}
        </li>
      )}
       // I'm handling the change event when a new amino acid is selected.
      // When a new value is selected, I'm navigating to a route corresponding to the selected amino acid's name.
      onChange={(event, newValue) => {
        navigate(`/${newValue.name}`);
      }}
    />
  );
}
