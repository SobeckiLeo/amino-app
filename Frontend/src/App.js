import React, { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import ClientRoutes from "./Components/ClientRoutes";
import InfoComponent from "./Components/InfoComponent";
import QuizComponent from "./Components/QuizComponent";
import Component from "./Components/Component";
import Footer from "./Components/Footer";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import "./index.css";

const App = ({ initialComponent }) => {
  // Defining the main App component and destructuring initialComponent and components from the props.
  const [currentComponent, setCurrentComponent] = useState(initialComponent);
  // I'm setting up a piece of state to manage which component should be currently rendered.

  // Here I'm adding a function that allows me to go back and forth between infocomponenta and quizcomponent.
  const switchComponent = (component) => {
    setCurrentComponent(component);
  };

  // This is where the currentComponent is rendered, and switchComponent is passed down to allow
  // child components to switch the currentComponent.
  return (
    <div className="App">
      <h1>Amino Acid Search</h1>
      {/*// Using Material-UI's Box component to handle layout properties:
      // An AppBar for navigation, with some custom styling.
      // A Toolbar for the navigation items (with custom styling).
      // Plus ComboBox to access specific amino acids which is being accesseed through the ClientRoutes.*/}
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="relative" sx={{ borderRadius: "20px" }}>
          <Toolbar sx={{ background: "#252d4a", borderRadius: "20px" }}>
            <Router>
              <ClientRoutes />
              {/*// This is where my main navigation links/buttons are defined.*/}
            </Router>
          </Toolbar>
        </AppBar>
      </Box>
      <div className="container">
        {/* // I'm using a container div to group and style content together.
        // If the currentComponent state is set to "InfoComponent", then I
        render the InfoComponent. // I'm also passing down the switchComponent
        function so that the InfoComponent can request a component switch. //
        Similarly, if the currentComponent state is set to "QuizComponent", then
    I render the QuizComponent. */}
        <Component>
          {currentComponent === "InfoComponent" && (
            <InfoComponent switchComponent={switchComponent} />
          )}
          {currentComponent === "QuizComponent" && (
            <QuizComponent switchComponent={switchComponent} />
          )}
        </Component>
        <Footer />
      </div>
    </div>
  );
};

export default App;
